from converter.errors import CodeError
from converter.status_codes import STATUS_CODES


class RedisBaseError(CodeError):
    ...


class RedisNotFoundError(RedisBaseError):
    _code = STATUS_CODES['NOT_FOUND']
