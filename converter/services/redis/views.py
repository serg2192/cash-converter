import asyncio
import decimal
import logging

from converter.services.redis import base
from converter.services.redis import schemas

logger = logging.getLogger(__name__)


async def convert(request, source, target, amount):
    outs = schemas.ConvertOutSchema()
    cache = request.app['redis_cache_pool']

    rate = await cache.hget(source, target, encoding='utf-8')
    if rate is None:
        logger.warning(f'No rates found converting "{source}" to "{target}".')
        raise base.RedisNotFoundError

    res = decimal.Decimal(amount) * decimal.Decimal(rate)
    return outs.dump({'result': res})


async def populate(request, inp):
    merge = inp['merge']
    rates = inp['rates']
    cache = request.app['redis_cache_pool']

    tasks_data = []
    for rate_data in rates:
        source = rate_data['source']
        target = rate_data['target']
        s2t_rate = rate_data['rate']
        t2s_rate = 1 / s2t_rate
        tasks_data.append((source, target, s2t_rate))
        tasks_data.append((target, source, t2s_rate))

    if not merge:
        # weakness: if one of update statements fails
        # whole transaction will be rolled back.
        # Update by pair may be better.
        all_keys = await cache.keys('*')
        tr = cache.multi_exec()
        tr.delete(*all_keys)
        for tdata in tasks_data:
            tr.hset(*tdata)
        await tr.execute()
    else:
        await asyncio.gather(
            *(cache.hset(*tdata) for tdata in tasks_data),
        )
    return
