from marshmallow import Schema, fields


class ConvertOutSchema(Schema):
    result = fields.Decimal(
        # places=2, todo check this
        as_string=True,
    )
