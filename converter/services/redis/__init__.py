import logging

import aioredis

logger = logging.getLogger(__name__)


async def on_startup(app):
    logger.info('Starting up: Redis')
    pool = await aioredis.create_redis_pool(**app['config']['redis_cache'])
    app['redis_cache_pool'] = pool


async def on_cleanup(app):
    logger.info('Cleaning up: Redis')
    app['redis_cache_pool'].close()
    await app['redis_cache_pool'].wait_closed()
