from converter.middlewares import loadreq
from converter.resources import schemas
from converter.services.redis import views as r_views


@loadreq(schemas.ConvertSchema, location='query')
async def convert(req, inp):
    return await r_views.convert(req, **inp)


@loadreq(schemas.DatabaseSchema, location='query_and_json')
async def database(req, inp):
    return await r_views.populate(req, inp)
