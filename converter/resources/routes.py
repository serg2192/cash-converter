from converter.resources import views


def set_routes(app):
    router = app.router

    convert_resource = router.add_resource(
        '/convert', name='convert'
    )
    convert_resource.add_route(
        'GET',
        views.convert
    )

    database_resource = router.add_resource(
        '/database', name='database'
    )
    database_resource.add_route(
        'POST',
        views.database
    )
