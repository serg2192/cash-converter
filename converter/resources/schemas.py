from marshmallow import Schema, fields, validate, EXCLUDE


class ConvertSchema(Schema):
    source = fields.String(
        required=True,
        allow_none=False,
        data_key='from',
        description='из валюты'
    )
    target = fields.String(
        required=True,
        allow_none=False,
        data_key='to',
        description='в валюту'
    )
    amount = fields.Float(
        required=True,
        allow_none=False,
        description='количество'
    )

    class Meta:
        strict = True
        unknown = EXCLUDE


class RateSchema(Schema):
    source = fields.String(
        required=True,
        allow_none=False,
        data_key='from',
        description='из валюты'
    )
    target = fields.String(
        required=True,
        allow_none=False,
        data_key='to',
        description='в валюту'
    )
    rate = fields.Float(
        required=True,
        allow_none=False,
        description='Курс'
    )


class DatabaseSchema(Schema):
    rates = fields.List(
        fields.Nested(RateSchema()),
        required=True,
        allow_none=False,
        description='в валюту'
    )

    merge = fields.Integer(  # todo: Boolean???
        required=True,
        allow_none=False,
        validate=validate.OneOf((0, 1)),
        description='Флаг сброса старых данных'
    )

    class Meta:
        strict = True
        unknown = EXCLUDE
