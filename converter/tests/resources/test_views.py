import typing

from aiohttp.test_utils import TestClient
import pytest


@pytest.fixture
async def populate_data(app, clear_cache):
    redis_client = app['redis_cache_pool']
    records = []

    async def init(data: typing.Sequence[typing.Sequence]):
        print(f'Initializing data: {data}')
        if data is None:
            return
        for key, field, value in data:
            await redis_client.hset(key, field, value)
            records.append(key)
    yield init
    # print(f'Cleaning data: {records}')
    # if records:
    #     await redis_client.delete(*records)


@pytest.fixture
async def clear_cache(app):
    yield
    print(f'Cleaning data.')
    await app['redis_cache_pool'].flushdb()


@pytest.mark.parametrize(
    "init_data,req_data,expected_res",
    [
        pytest.param(
            None,
            {},
            {
                "data": None,
                "metadata": {
                    "code": 100,
                    "message": "Никогда такого не было и вот опять...",
                    "details": [
                        {
                            'amount': ['Missing data for required field.'],
                            'from': ['Missing data for required field.'],
                            'to': ['Missing data for required field.']
                        }
                    ]
                }
            },
            id='invalid parameters passed'
        ),
        pytest.param(
            None,
            {'from': 'RUB', 'to': 'USD', 'amount': 2},
            {'data': None, 'metadata': {'code': 101, 'details': None, 'message': ''}},
            id='no rates found'
        ),
        pytest.param(
            [('RUB', 'USD', 60)],
            {'from': 'RUB', 'to': 'USD', 'amount': 2},
            {'data': {'result': '120'}, 'metadata': {'code': 0, 'details': None, 'message': ''}},
            id='successful convert'
        ),
    ]
)
async def test_convert(
        cli: TestClient,
        populate_data,
        init_data,
        req_data,
        expected_res
):
    await populate_data(init_data)
    res = await cli.request('GET', '/convert', params=req_data)
    res_json = await res.json()
    assert res_json == expected_res


@pytest.mark.parametrize(
    "init_data,req_data,req_params,expected_res,expected_data",
    [
        pytest.param(
            None,  # предзаполнить данными
            {},  # пэйлоад реквеста
            {},  # параметры реквеста
            {  # ожидаемый ответ сервиса

                "data": None,
                "metadata": {
                    "code": 100,
                    "message": "Переданы неверные входные параметры",
                    "details": [
                        {
                            'merge': ['Missing data for required field.'],
                            'rates': ['Missing data for required field.']
                        }
                    ]
                }
            },
            None,  # ожидаемые данные в хранилище
            id='invalid parameters passed'
        ),
        pytest.param(
            [('RUB', 'USD', 60)],
            {"rates": [{"from": "EUR", "to": "RUB", "rate": 78.50}]},
            {"merge": 0},
            {"data": None, "metadata": {"code": 0, "message": "Успех", "details": None}},
            {"EUR": {"RUB": "78.5"}, "RUB": {"EUR": "0.012738853503184714"}},
            id='replace the database'
        ),
        pytest.param(
            [('RUB', 'USD', 60)],
            {"rates": [{"from": "EUR", "to": "RUB", "rate": 78.50}]},
            {"merge": 1},
            {"data": None, "metadata": {"code": 0, "message": "Успех", "details": None}},
            {"EUR": {"RUB": "78.5"}, "RUB": {"EUR": "0.012738853503184714", "USD": "60"}},
            id='update the database'
        ),
        pytest.param(
            [('RUB', 'USD', 60)],
            {"rates": [
                {"from": "EUR", "to": "RUB", "rate": 78.50},
                {"from": "RUB", "to": "EUR", "rate": 0.0001},
            ]},
            {"merge": 1},
            {"data": None, "metadata": {"code": 0, "message": "Успех", "details": None}},
            {"EUR": {"RUB": "10000.0"}, "RUB": {"EUR": "0.0001", "USD": "60"}},
            id='update the database. v2'
        ),
    ]
)
async def test_database(
        cli: TestClient,
        populate_data,
        init_data,
        req_data,
        req_params,
        expected_res,
        expected_data
):
    await populate_data(init_data)
    res = await cli.request('POST', '/database', json=req_data, params=req_params)
    res_json = await res.json()

    await check_with_reference(
        cli.app['redis_cache_pool'],
        expected_data
    )

    assert res_json == expected_res


async def check_with_reference(redis_cache, reference):
    if reference:
        for key, values in reference.items():
            ref = await redis_cache.hgetall(key, encoding='utf-8')
            assert ref == values  # todo; almost equal
