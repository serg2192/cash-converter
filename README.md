**Запуск проекта**

В корне проекта выполнить:
```bash
docker-compose -f ./docker-compose.yaml up
```
Проект будет доступен на 0.0.0.0:8080

**Важные заметки по реализации:**
1. Все попытки вызова методов будут возвращать либо 200(с каким-либо кодом в metadata) 
либо 500(если возникло какое то исключение)
1. Курсы обмена валют хранятся в docker volume (curr_conv_rates). 
Удалить будет необходимо самостоятельно.
1. Метод создания/обновления курса будет создавать запись как
 для прямого размена, так и для обратного. 
 Таким образом вызов 
 ```bash
curl -w '\n' -iX POST 'http://0.0.0.0:8080/database?merge=1' -H 'Content-Type:application/json' -d '{"rates":[{"from":"EUR","to":"RUB","rate":78.50}]}'
``` 
создаст запись для конвертации из евро в рубли и из рублей в евро.

Примеры вызова методов: 
1. Конвертация
Успешная
```bash
curl -w '\n' -iX GET 'http://0.0.0.0:8080/convert?from=RUB&to=USD&amount=2'
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 101
Date: Sun, 14 Jun 2020 21:09:39 GMT
Server: Python/3.7 aiohttp/3.6.2

{"data": {"result": "0.028677946659019216"}, "metadata": {"code": 0, "message": "", "details": null}}
```
Невалидные входные параметры
```bash
curl -w '\n' -iX GET 'http://0.0.0.0:8080/convert?from=RUB&to=USD'
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 184
Date: Sun, 14 Jun 2020 21:10:57 GMT
Server: Python/3.7 aiohttp/3.6.2

{"data": null, "metadata": {"code": 100, "message": "Никогда такого не было и вот опять...", "details": [{"amount": ["Missing data for required field."]}]}}
```
1. Создание курса обмена
Успех
```bash
curl -w '\n' -iX POST 'http://0.0.0.0:8080/database?merge=1' -H 'Content-Type:application/json' -d '{"rates":[{"from":"EUR","to":"RUB","rate":78.50}]}'
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 81
Date: Sun, 14 Jun 2020 21:13:14 GMT
Server: Python/3.7 aiohttp/3.6.2

{"data": null, "metadata": {"code": 0, "message": "Успех", "details": null}}
```
Невалидные входные параметры
```bash
curl -w '\n' -iX POST 'http://0.0.0.0:8080/database?merge=1' -H 'Content-Type:application/json' -d '{"rates":[{"from":"EUR","t":"RUB","rate":78.50}]}'
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: 225
Date: Sun, 14 Jun 2020 21:13:57 GMT
Server: Python/3.7 aiohttp/3.6.2

{"data": null, "metadata": {"code": 100, "message": "Переданы неверные входные параметры", "details": [{"rates": {"0": {"to": ["Missing data for required field."], "t": ["Unknown field."]}}}]}}
```